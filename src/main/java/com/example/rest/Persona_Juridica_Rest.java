package com.example.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Persona_Juridica;
import com.example.service.Persona_Juridica_Service;

@RestController
@RequestMapping("/persona_juridica/")
public class Persona_Juridica_Rest {
	@Autowired
	private Persona_Juridica_Service juridicaService;

	@GetMapping
	public ResponseEntity<List<Persona_Juridica>> getAllPersonaJuridica() {
		return ResponseEntity.ok(juridicaService.findAll());
	}

	@PostMapping
	public ResponseEntity<Persona_Juridica> savePersonaJuridica(@RequestBody Persona_Juridica personaJ) {
		try {
			Persona_Juridica my_person = juridicaService.save(personaJ);
			return ResponseEntity.created(new URI("/persona_juridica/" + my_person.getId())).body(my_person);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

	}
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deletePersonaJuridica(@PathVariable Long id) {
	    try {
	        juridicaService.deleteById(id);
	        return ResponseEntity.noContent().build();
	    } catch (Exception e) {
	        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	    }
	}

}
