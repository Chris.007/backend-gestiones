package com.example.rest;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Usuario_Financiero;
import com.example.service.Usuario_Financiero_Service;

@RestController
@RequestMapping("/usuario_financiero/")
public class Usuario_Financiero_Rest {
	
	@Autowired
	private Usuario_Financiero_Service financieroService;
	
	@GetMapping
	private ResponseEntity<List<Usuario_Financiero>> getAllUsuarioFinanciero(){
		return ResponseEntity.ok(financieroService.findAll());
	}
}
