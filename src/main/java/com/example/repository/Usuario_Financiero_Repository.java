package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Usuario_Financiero;

public interface Usuario_Financiero_Repository extends JpaRepository<Usuario_Financiero, Long>{

}
