package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Persona_Natural;

public interface Persona_Natural_Repository extends JpaRepository<Persona_Natural, Long> {

}
