package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Persona_Juridica;

public interface Persona_Juridica_Repository extends JpaRepository<Persona_Juridica, Long>{

}
