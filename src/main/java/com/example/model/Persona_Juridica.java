package com.example.model;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table (name ="persona_juridica")
public class Persona_Juridica {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@OneToOne
    @JoinColumn(name = "id_usuario_financiero")
    private Usuario_Financiero usuarioFinanciero; 
	
	@Column (nullable = false, length = 100)
	private String nombres;
	
	@Column (nullable = false, length = 100)
	private String razon_social;
	
	@Column (nullable = false, length = 20)
	private String nit;
	
	@Column (nullable = false, length = 60)
	private String direccion;
	
	@Column (nullable = false, length = 20)
	private String telefono;
	
	public Persona_Juridica() {}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Usuario_Financiero getUsuarioFinanciero() {
		return usuarioFinanciero;
	}

	public void setUsuarioFinanciero(Usuario_Financiero usuarioFinanciero) {
		this.usuarioFinanciero = usuarioFinanciero;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getRazon_social() {
		return razon_social;
	}

	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	
}
