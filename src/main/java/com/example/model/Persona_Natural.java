package com.example.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "persona_natural")

public class Persona_Natural {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@OneToOne
	@JoinColumn(name = "id_usuario_financiero")
	private Usuario_Financiero usuarioFinanciero;

	@Column(nullable = false, length = 100)
	private String nombres;

	@Column(nullable = false, length = 60)
	private String primer_apellido;

	@Column(nullable = false, length = 60)
	private String segundo_apellido;

	@Column(nullable = false, length = 20)
	private String nro_documento_identidad;

	@Column(nullable = false, length = 20)
	private String telefono;

	@Column(nullable = false, length = 40)
	private String direccion_domicilio;

	public Persona_Natural() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Usuario_Financiero getUsuarioFinanciero() {
		return usuarioFinanciero;
	}

	public void setUsuarioFinanciero(Usuario_Financiero usuarioFinanciero) {
		this.usuarioFinanciero = usuarioFinanciero;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getPrimer_apellido() {
		return primer_apellido;
	}

	public void setPrimer_apellido(String primer_apellido) {
		this.primer_apellido = primer_apellido;
	}

	public String getSegundo_apellido() {
		return segundo_apellido;
	}

	public void setSegundo_apellido(String segundo_apellido) {
		this.segundo_apellido = segundo_apellido;
	}

	public String getNro_documento_identidad() {
		return nro_documento_identidad;
	}

	public void setNro_documento_identidad(String nro_documento_identidad) {
		this.nro_documento_identidad = nro_documento_identidad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion_domicilio() {
		return direccion_domicilio;
	}

	public void setDireccion_domicilio(String direccion_domicilio) {
		this.direccion_domicilio = direccion_domicilio;
	}

}
